import zmq
import zmq.asyncio
import asyncio
import socket
import uuid


zmq_dealer = None
identify = '{}-{}'.format(socket.gethostname(), uuid.uuid4().hex[:6]).encode()


async def send_worker():
    while True:
        await zmq_dealer.send_multipart([b'ALIVE'])
        await asyncio.sleep(5)


async def receive_worker():
    while True:
        try:
            event, *payload = await zmq_dealer.recv_multipart()
        except Exception as e:
            print('error receive data', e)
            continue

        if event == b'DATA':
            eratosfen = payload.pop()
            # print('------ received fibo', int(fibo.decode('utf-8')))
            print('------ received eratosfen', eratosfen.decode('utf-8'))


def main():
    global zmq_dealer
    loop = asyncio.get_event_loop()

    context = zmq.asyncio.Context.instance()
    zmq_dealer = context.socket(zmq.DEALER)
    zmq_dealer.set(zmq.IDENTITY, identify)
    zmq_dealer.connect('tcp://hw_producer:8689')
    asyncio.ensure_future(send_worker())
    asyncio.ensure_future(receive_worker())

    try:
        loop.run_forever()
    except Exception as e:
        print('event loop error', e)
    finally:
        loop.close()


if __name__ == '__main__':
    main()


