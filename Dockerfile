FROM python:3.7.1-stretch

RUN apt-get update && apt-get install -qq -y --no-install-recommends \
  telnet \
  net-tools

COPY ./requirements.txt /requirements.txt

RUN pip install --upgrade pip \
&& pip install -U -r requirements.txt

COPY ./ /app
