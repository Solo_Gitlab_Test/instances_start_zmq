import random
import time
import asyncio
import zmq
import zmq.asyncio

zmq_router = None
dealers = {}


def get_eratosfen_list(n):
    n += 1

    a = [True] * n
    a[0] = a[1] = False

    for k in range(2, n):
        if a[k]:
            for m in range(2 * k, n, k):
                a[m] = False

    return a


def get_eratosfen_sublist_as_numbers(n):
    # возвращает вырезанный кусок списка - в нем только простые числа
    main_eratosfen_list = get_eratosfen_list(n)
    lst = []
    for x in range(n):
        if main_eratosfen_list[x]:
            lst.append(x)
    return lst


def fibo(n):
    if n in [0, 1]:
        return 1

    return fibo(n - 1) + fibo(n - 2)


async def send_worker():
    while True:
        # eratosfen_data = get_eratosfen_list(random.randint(10, 50))
        eratosfen_data = get_eratosfen_sublist_as_numbers(random.randint(10, 50))
        try:
            for identify in dealers.keys():
                # print("dealers.keys.len: ", dealers.keys().__len__()) # количество подключенных клиентов
                await zmq_router.send_multipart([identify, b'DATA', str(eratosfen_data).encode()], copy=False)
                print('------ sent eratosfen numbers', eratosfen_data)
        except Exception as e:
            print('======error send message', e)
        await asyncio.sleep(3)


async def receive_worker():
    while True:
        try:
            identify, event, *payload = await zmq_router.recv_multipart()
        except Exception as e:
            print('error receive data', e)
            continue

        dealers[identify] = time.time()


async def cron_worker():
    while True:
        for identify, last_message_time in dealers.copy().items():
            if time.time() - last_message_time > 10:
                dealers.pop(identify, None)

        await asyncio.sleep(5)


def main():
    global zmq_router
    loop = asyncio.get_event_loop()

    context = zmq.asyncio.Context.instance()
    zmq_router = context.socket(zmq.ROUTER)
    zmq_router.bind('tcp://*:8689')
    asyncio.ensure_future(receive_worker())
    asyncio.ensure_future(send_worker())
    asyncio.ensure_future(cron_worker())

    try:
        loop.run_forever()
    except Exception as e:
        print('event loop error', e)
    finally:
        loop.close()


if __name__ == '__main__':
    main()
